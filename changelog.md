ChangeLog
=========

1.0.0 (2019-01-13)
-------------------------
* Added experimental support for Chrome Extension
* Minify some files
* Fixed some small bug for initial sync

0.9.15 (2018-12-23)
-------------------------
* Added PWA mode for mobile browsers
* Moved configuration to separate file

0.9.14 (2018-12-03)
-------------------------
* added search function

0.9.13 (2018-12-03)
-------------------------
* fixed dialog hiding
* fixed unique id issue
* added function to select device name

0.9.12 (2018-12-01)
-------------------------
* added edit function
* added move function
* cleaned css

0.9.11 (2018-11-27)
-------------------------
* better mobile browser detection
* added user management
* removed import for file

0.9.10 (2018-11-25)
-------------------------
* check for already existing bookmarks added

0.9.9 (2018-11-24)
-------------------------
* added plugin import function
* added plugin export function

0.9.8 (2018-11-20)
-------------------------
* changed lastlogin recording

0.9.7 (2018-11-19)
-------------------------
* added mobil theme
* added mobile scripting

0.9.6 (2018-11-18)
-------------------------
* added startup sync for firefox
* changed login process

0.9.5 (2018-10-03)
-------------------------
* changed WebGUI to AJAX calls